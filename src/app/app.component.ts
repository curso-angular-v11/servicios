import { Component } from '@angular/core';
import { Persona } from './models/persona.model';
import { PersonasService } from './services/personas.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [PersonasService]
})
export class AppComponent {
  
  arrPersonas: Persona[];
  constructor(private personaService: PersonasService){}

  ngOnInit(){
    this.arrPersonas = this.personaService.getAll();
  }

  onClick(){
    this,this.personaService.create(new Persona('Nueva', 'Garcia', 45, true))
  }

  async onClickActivas(){
    // this.personaService.activos()
    //   .then((arrTempPersonas) => {console.log(arrTempPersonas);})
    //   .catch(error => console.log(error))

    try{
      this.arrPersonas = await this.personaService.activosV2();
    }catch(error) {
      console.log(error);
    }
  }
}
