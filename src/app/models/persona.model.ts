export class Persona{
    nombre: string;
    apellidos: string;
    edad: number;
    activo: boolean;

    constructor(pNombre: string, pApellido: string, pEdad: number, pActivo: boolean){
        this.nombre = pNombre;
        this.apellidos = pApellido;
        this.edad = pEdad;
        this.activo = pActivo;
    }
}